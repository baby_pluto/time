package ru.am;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try (Scanner scn = new Scanner(System.in)) {
            DateTimeFormatter f = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.ENGLISH);

            LocalDate date = LocalDate.parse(scn.nextLine(), f);

            if (date.isLeapYear()) System.out.println("Это високосный год.");
            else System.out.println("Это не високосный год.");
        } catch (DateTimeParseException e) {
            System.out.println("Дата введена неверно.");
        }
    }
}